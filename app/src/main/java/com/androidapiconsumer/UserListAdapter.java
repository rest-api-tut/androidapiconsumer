package com.androidapiconsumer;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.androidapiconsumer.services.UserSevice;

import java.util.List;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder> {

    private final List<User> userList;
    private final Activity activity;
    private final UserSevice userSevice;

    public UserListAdapter(List<User> userList, Activity activity, UserSevice userSevice) {
        this.userList = userList;
        this.activity = activity;
        this.userSevice = userSevice;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_view_element, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User user = userList.get(position);
        holder.counter.setText(String.valueOf(position + 1));
        String name = user.getFirstName() + " " + user.getLastName();
        holder.name.setText(name);
        holder.showBtn.setOnClickListener(view -> userSevice.showUser(activity, user.getId()));
        holder.deleteBtn.setOnClickListener(view -> userSevice.deleteUser(activity, user.getId()));
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView counter;
        private final TextView name;
        private final ImageButton showBtn, deleteBtn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            counter = itemView.findViewById(R.id.countLabel);
            name = itemView.findViewById(R.id.nameLabel);
            showBtn = itemView.findViewById(R.id.showBtn);
            deleteBtn = itemView.findViewById(R.id.deleteBtn);
        }
    }
}

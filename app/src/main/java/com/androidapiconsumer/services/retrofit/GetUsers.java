package com.androidapiconsumer.services.retrofit;

import com.androidapiconsumer.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface GetUsers {

    @GET("/api/users")
    @Headers({
            "Accept: application/json"
    })
    Call<List<User>> getUsers();
}

package com.androidapiconsumer.services.retrofit;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Path;

public interface DeleteUser {

    @DELETE("api/users/{id}")
    Call<Void> deleteUser(@Path("id") Long id);
}

package com.androidapiconsumer.services.retrofit;

import com.androidapiconsumer.User;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface GetUser {

    @GET("api/users/{id}")
    @Headers({
            "Accept: application/json"
    })
    Call<User> getUser(@Path("id") Long id);
}

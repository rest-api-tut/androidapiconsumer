package com.androidapiconsumer.services.retrofit;

import com.androidapiconsumer.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface PostUser {

    @POST("api/users")
    Call<User> postUser(@Body User user);
}

package com.androidapiconsumer.services;

import android.app.Activity;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.androidapiconsumer.R;
import com.androidapiconsumer.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public interface UserSevice {
    public static final String BASE_URL = "http://10.0.2.2:8080/api/users";
    void showUser(Activity activity, Long id);
    void addUser(Activity activity);
    void deleteUser(Activity activity, Long id);
    void getUsers(RecyclerView recyclerView, Activity activity);

    static List<User> responseToUserList(JSONArray array) {
        List<User> users = new ArrayList<>();

        for(int i = 0; i < array.length(); i++) {
            try {
                JSONObject userJ =array.getJSONObject(i);
                users.add(new User(
                    userJ.getLong("id"),
                    userJ.getString("firstName"),
                    userJ.getString("lastName"),
                    userJ.getInt("age")
                ));
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }
        return users;
    }

    static User getUserFromView(Activity activity) {
        TextView firstName = activity.findViewById(R.id.eFirstName);
        TextView lastName = activity.findViewById(R.id.eLastName);
        TextView age = activity.findViewById(R.id.eAge);

        return new User(firstName.getText().toString(),
                lastName.getText().toString(),
                Integer.parseInt(age.getText().toString()));
    }

    static void clearForm(Activity activity) {
        TextView firstName = activity.findViewById(R.id.eFirstName);
        TextView lastName = activity.findViewById(R.id.eLastName);
        TextView age = activity.findViewById(R.id.eAge);

        firstName.setText("");
        lastName.setText("");
        age.setText("");
    }

    static void updateView(Activity activity, int visibility, String firstName, String lastName, int age) {
        ConstraintLayout userLayout = activity.findViewById(R.id.showUserLayout);

        TextView showFirstName = activity.findViewById(R.id.showFirstName);
        TextView showLastName = activity.findViewById(R.id.showLastName);
        TextView showAge = activity.findViewById(R.id.showAge);

        userLayout.setVisibility(visibility);
        showFirstName.setText(firstName);
        showLastName.setText(lastName);
        showAge.setText(String.valueOf(age));

    }
}

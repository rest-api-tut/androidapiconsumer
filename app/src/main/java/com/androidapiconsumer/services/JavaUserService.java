package com.androidapiconsumer.services;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.androidapiconsumer.R;
import com.androidapiconsumer.User;
import com.androidapiconsumer.UserListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class JavaUserService implements UserSevice{
    private final Executor executor = Executors.newSingleThreadExecutor();
    private final Handler handler = new Handler(Looper.getMainLooper());
    @Override
    public void showUser(Activity activity, Long id) {
        executor.execute(() -> {
            HttpURLConnection client = null;
            try {
                URL url = new URL(BASE_URL + "/" + id);
                client = (HttpURLConnection) url.openConnection();
                client.setRequestMethod("GET");
                client.setRequestProperty("Accept", "application/json");

                try(BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()))) {
                    StringBuilder response = new StringBuilder();
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        response.append(line);
                    }
                    JSONObject userJSON = (JSONObject) new JSONTokener(response.toString()).nextValue();

                    handler.post(() -> {
                        try {
                            UserSevice.updateView(activity,
                                    View.VISIBLE,
                                    userJSON.getString("firstName"),
                                    userJSON.getString("lastName"),
                                    userJSON.getInt("age"));
                        } catch (JSONException e) {
                            throw new RuntimeException(e);
                        }
                    });
                }
            } catch (IOException | JSONException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public void addUser(Activity activity) {
        User user = UserSevice.getUserFromView(activity);
        executor.execute(() -> {
            HttpURLConnection client = null;

            try {
                JSONObject userJ = new JSONObject();
                userJ.put("firstName", user.getFirstName());
                userJ.put("lastName", user.getLastName());
                userJ.put("age", user.getAge());

                URL url = new URL(BASE_URL);
                client = (HttpURLConnection) url.openConnection();
                client.setRequestMethod("POST");
                client.setRequestProperty("Content-Type", "application/json");

                OutputStream out = new BufferedOutputStream(client.getOutputStream());
                out.write(userJ.toString().getBytes(StandardCharsets.UTF_8));
                out.flush();
                out.close();

                if (client.getResponseCode() == 201) {
                    getUsers(activity.findViewById(R.id.userListView), activity);
                }

                UserSevice.clearForm(activity);
            } catch (IOException | JSONException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public void deleteUser(Activity activity, Long id) {
        executor.execute(() -> {
            HttpURLConnection client = null;
            try {
                URL url = new URL(BASE_URL + "/" + id);
                client = (HttpURLConnection) url.openConnection();
                client.setRequestMethod("DELETE");
                if (client.getResponseCode() == 200 || client.getResponseCode() == 204) {
                    getUsers(activity.findViewById(R.id.userListView), activity);
                }

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public void getUsers(RecyclerView recyclerView, Activity activity) {
        executor.execute(() -> {
            HttpURLConnection client = null;
            try {
                URL url = new URL(BASE_URL);
                client = (HttpURLConnection) url.openConnection();
                client.setRequestMethod("GET");
                client.setRequestProperty("Accept", "application/json");

                try(BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()))) {
                    StringBuilder response = new StringBuilder();
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        response.append(line);
                    }
                    JSONArray usersJSON = (JSONArray) new JSONTokener(response.toString()).nextValue();
                    List<User> users = UserSevice.responseToUserList(usersJSON);

                    handler.post(() -> recyclerView.setAdapter(new UserListAdapter(users,activity,this)));
                }
            } catch (IOException | JSONException e) {
                throw new RuntimeException(e);
            }
        });
    }
}

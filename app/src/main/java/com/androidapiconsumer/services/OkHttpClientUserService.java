package com.androidapiconsumer.services;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.androidapiconsumer.R;
import com.androidapiconsumer.User;
import com.androidapiconsumer.UserListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class OkHttpClientUserService implements UserSevice{
    private final Executor executor = Executors.newSingleThreadExecutor();
    private final Handler handler = new Handler(Looper.getMainLooper());
    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    private final OkHttpClient httpClient = new OkHttpClient();
    @Override
    public void showUser(Activity activity, Long id) {
        Request request = new Request.Builder()
                .get()
                .url(BASE_URL + "/" + id)
                .build();

        executor.execute(() -> {
            try(Response response = httpClient.newCall(request).execute()){
                if (response.body() != null) {
                    JSONObject userJSON = (JSONObject) new JSONTokener(response.body().string()).nextValue();
                    handler.post(() -> {
                        try {
                            UserSevice.updateView(activity,
                                    View.VISIBLE,
                                    userJSON.getString("firstName"),
                                    userJSON.getString("lastName"),
                                    userJSON.getInt("age"));
                        } catch (JSONException e) {
                            throw new RuntimeException(e);
                        }
                    });
                }
            } catch (IOException | JSONException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public void addUser(Activity activity) {
        User user = UserSevice.getUserFromView(activity);
        JSONObject userJ = new JSONObject();
        try {
            userJ.put("firstName", user.getFirstName());
            userJ.put("lastName", user.getLastName());
            userJ.put("age", user.getAge());

            RequestBody requestBody = RequestBody.create(userJ.toString(), JSON);
            Request request = new Request.Builder()
                    .post(requestBody)
                    .url(BASE_URL)
                    .build();

            executor.execute(() -> {
                try(Response response = httpClient.newCall(request).execute()){
                    if (response.body() != null) {
                        UserSevice.clearForm(activity);
                        getUsers(activity.findViewById(R.id.userListView), activity);
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });



        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteUser(Activity activity, Long id) {
        Request request = new Request.Builder()
                .delete()
                .url(BASE_URL + "/" + id)
                .build();

        executor.execute(() -> {
            try(Response ignored = httpClient.newCall(request).execute()){
                getUsers(activity.findViewById(R.id.userListView), activity);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public void getUsers(RecyclerView recyclerView, Activity activity) {
        Request request = new Request.Builder()
                .get()
                .url(BASE_URL)
                .build();

        executor.execute(() -> {
            try(Response response = httpClient.newCall(request).execute()){
                if (response.body() != null) {
                    JSONArray usersJSON = (JSONArray) new JSONTokener(response.body().string()).nextValue();
                    List<User> users = UserSevice.responseToUserList(usersJSON);

                    handler.post(() -> recyclerView.setAdapter(new UserListAdapter(users,activity,this)));
                }
            } catch (IOException | JSONException e) {
                throw new RuntimeException(e);
            }
        });

    }
}

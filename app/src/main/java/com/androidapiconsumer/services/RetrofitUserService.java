package com.androidapiconsumer.services;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.androidapiconsumer.R;
import com.androidapiconsumer.User;
import com.androidapiconsumer.UserListAdapter;
import com.androidapiconsumer.services.retrofit.DeleteUser;
import com.androidapiconsumer.services.retrofit.GetUser;
import com.androidapiconsumer.services.retrofit.GetUsers;
import com.androidapiconsumer.services.retrofit.PostUser;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitUserService implements UserSevice{
    private static final String RETROFIT_BASE_URL = "http://10.0.2.2:8080/";
    private final Executor executor = Executors.newSingleThreadExecutor();
    private final Handler handler = new Handler(Looper.getMainLooper());
    private static final Retrofit RETROFIT = new Retrofit.Builder()
            .baseUrl(RETROFIT_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    @Override
    public void showUser(Activity activity, Long id) {
        GetUser getUserService = RETROFIT.create(GetUser.class);
        Call<User> userCall = getUserService.getUser(id);

        executor.execute(() -> {
            try {
                User user = userCall.execute().body();
                handler.post(() -> {
                    UserSevice.updateView(activity,
                            View.VISIBLE,
                            user.getFirstName(),
                            user.getLastName(),
                            user.getAge());
                });
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public void addUser(Activity activity) {
        User userFromView = UserSevice.getUserFromView(activity);
        PostUser postUserService = RETROFIT.create(PostUser.class);

        executor.execute(() -> {
            try {
                if(postUserService.postUser(userFromView).execute().code() == 201) {
                    UserSevice.clearForm(activity);
                    getUsers(activity.findViewById(R.id.userListView), activity);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public void deleteUser(Activity activity, Long id) {
        DeleteUser deleteUserService = RETROFIT.create(DeleteUser.class);
        executor.execute(() -> {
            try {
                deleteUserService.deleteUser(id).execute();
                getUsers(activity.findViewById(R.id.userListView), activity);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public void getUsers(RecyclerView recyclerView, Activity activity) {
        GetUsers getUsersService = RETROFIT.create(GetUsers.class);
        Call<List<User>> userCall = getUsersService.getUsers();;

        executor.execute(() -> {
            try {
                List<User> users = userCall.execute().body();
                handler.post(() -> recyclerView.setAdapter(new UserListAdapter(users,activity,this)));

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }
}

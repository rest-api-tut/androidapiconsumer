package com.androidapiconsumer.services;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidapiconsumer.R;
import com.androidapiconsumer.User;
import com.androidapiconsumer.UserListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.List;

public class VolleyUserService implements UserSevice{

    private final Handler handler = new Handler(Looper.getMainLooper());
    @Override
    public void showUser(Activity activity, Long id) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                BASE_URL + "/" + id,
                null,
                response -> handler.post(() -> {
                    try {
                        UserSevice.updateView(activity,
                                View.VISIBLE,
                                response.getString("firstName"),
                                response.getString("lastName"),
                                response.getInt("age"));
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                }),
                new DummyErrorResponse()
        );
        Volley.newRequestQueue(activity).add(jsonObjectRequest);
    }

    @Override
    public void addUser(Activity activity) {
        User user = UserSevice.getUserFromView(activity);
        JSONObject userJ = new JSONObject();
        try {
            userJ.put("firstName", user.getFirstName());
            userJ.put("lastName", user.getLastName());
            userJ.put("age", user.getAge());
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                BASE_URL,
                userJ,
                response -> {
                    UserSevice.clearForm(activity);
                    getUsers(activity.findViewById(R.id.userListView), activity);
                },
                new DummyErrorResponse()
        );
        Volley.newRequestQueue(activity).add(jsonObjectRequest);
    }

    @Override
    public void deleteUser(Activity activity, Long id) {
        StringRequest stringRequest = new StringRequest(
                Request.Method.DELETE,
                BASE_URL + "/" + id,
                response -> getUsers(activity.findViewById(R.id.userListView), activity),
                new DummyErrorResponse()
        );
        Volley.newRequestQueue(activity).add(stringRequest);
    }

    @Override
    public void getUsers(RecyclerView recyclerView, Activity activity) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                BASE_URL,
                null,
                response -> {
                    List<User> users = UserSevice.responseToUserList(response);
                    handler.post(() -> recyclerView.setAdapter(new UserListAdapter(users, activity, VolleyUserService.this )));
                },
                new DummyErrorResponse()
        );

        Volley.newRequestQueue(activity).add(jsonArrayRequest);
    }

    class DummyErrorResponse implements Response.ErrorListener{

        @Override
        public void onErrorResponse(VolleyError error) {

        }
    }
}

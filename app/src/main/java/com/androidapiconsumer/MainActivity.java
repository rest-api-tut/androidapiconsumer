package com.androidapiconsumer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.androidapiconsumer.services.JavaUserService;
import com.androidapiconsumer.services.OkHttpClientUserService;
import com.androidapiconsumer.services.RetrofitUserService;
import com.androidapiconsumer.services.UserSevice;
import com.androidapiconsumer.services.VolleyUserService;

public class MainActivity extends AppCompatActivity {

    private final UserSevice userSevice = new RetrofitUserService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView userListView = findViewById(R.id.userListView);
        userListView.setLayoutManager(new LinearLayoutManager(this));
        userSevice.getUsers(userListView, this);

        Button addUserBtn = findViewById(R.id.addUserBtn);
        addUserBtn.setOnClickListener(v -> {
            userSevice.addUser(this);
        });
    }
}